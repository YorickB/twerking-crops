package Spigot.TwerkingCrops.Commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import Spigot.TwerkingCrops.Core;
import Spigot.TwerkingCrops.ToolBox;
import Spigot.TwerkingCrops.Enchantments.MainEnchants;

/*
 * Created by Yorick, Last modified on: 24-4-2018
 */
public class BookFunctie implements CommandExecutor {
  public static String Enchant = "Empty";
  public static String Level = "0";
  private FileConfiguration getConfig = Core.getInstance().getConfig();
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (commandLabel.equalsIgnoreCase("book"))
    {
      if (!sender.hasPermission("Twerk.Staff"))
      {
        sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.NoPerms")));
        return true;
      }
      if ((sender instanceof Player))
      {
        if (args.length <= 1)
        {
          sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.Error")));
          return true;
        }
        Enchant = args[0];
        Level = args[1];
        if (!Core.getInstance().Enchants.containsKey(Enchant))
        {
          sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.Enchant")));
          return true;
        }
        if ((!Level.equals("1")) && (!Level.equals("2")) && (!Level.equals("3")))
        {
          sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.Level")));
          return true;
        }
        Player player = (Player)sender;
        
        ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
        /*ItemMeta meta = item.getItemMeta();
        
        ArrayList<String> lore = new ArrayList<String>();
        if (Level.equals("1")) {
          lore.add(ChatColor.GRAY + Enchant + " I");
          meta.addEnchant(MainEnchants.ench, 1, true);;
        } else if (Level.equals("2")) {
          lore.add(ChatColor.GRAY + Enchant + " II");
          meta.addEnchant(MainEnchants.ench, 2, true);
        } else if (Level.equals("3")) {
          lore.add(ChatColor.GRAY + Enchant + " III");
          meta.addEnchant(MainEnchants.ench, 3, true);
        }
        
        meta.setLore(lore);
        item.setItemMeta(meta);*/
        
        ItemStack ResultItem = NewItemStack(item, 1);
        player.getInventory().setItem(player.getInventory().firstEmpty(), ResultItem);
        
        sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.Succes")));
        return true;
      }
      sender.sendMessage(ToolBox.cc(this.getConfig.getString("Messages.Book.PlayerOnly")));
      return true;
    }
    return false;
  }
  
  private ArrayList<String> loreEnch = new ArrayList<String>();
  private ItemStack NewItemStack(ItemStack ResultItem, int ResultItemEnchant) {
	  ItemStack item4 = new ItemStack(ResultItem.getType()); 
	  ItemMeta meta = item4.getItemMeta();
	  
	  meta.addEnchant(MainEnchants.ench, ResultItemEnchant, true);
	  loreEnch.clear();
	  
  	  if (ResultItemEnchant == 1)
        loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " I");
      if (ResultItemEnchant == 2)
        loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " II");
      if (ResultItemEnchant == 3)
        loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " III");
      
      meta.setLore(this.loreEnch);
      item4.setItemMeta(meta);
      return item4;
  }
}
