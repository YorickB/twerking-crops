package Spigot.TwerkingCrops.Enchantments;

import java.lang.reflect.Field;
import java.util.HashMap;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class MainEnchants
  extends JavaPlugin
  implements Listener
{
  public static CustomEnchantment ench = new CustomEnchantment(101);
  
  public static void Enable() {}
  
  @SuppressWarnings("unlikely-arg-type")
public static void Disable()
  {
    try
    {
      Field byIdField = Enchantment.class.getDeclaredField("byId");
      Field byNameField = Enchantment.class.getDeclaredField("byName");
      
      byIdField.setAccessible(true);
      byNameField.setAccessible(true);
      
      HashMap<Integer, Enchantment> byId = new HashMap<Integer, Enchantment>();
      HashMap<Integer, Enchantment> byName = new HashMap<Integer, Enchantment>();
      if (byId.containsKey(Integer.valueOf(ench.getId()))) {
        byId.remove(Integer.valueOf(ench.getId()));
      }
      if (byName.containsKey(ench.getName())) {
        byName.remove(ench.getName());
      }
    }
    catch (Exception localException) {}
  }
  
  @SuppressWarnings("unused")
  private static void loadEnchantments()
  {
    try
    {
      try
      {
        Field f = Enchantment.class.getDeclaredField("acceptingNew");
        f.setAccessible(true);
        f.set(null, Boolean.valueOf(true));
      }
      catch (NoSuchFieldException|SecurityException|IllegalArgumentException|IllegalAccessException e)
      {
        e.printStackTrace();
      }
      try
      {
        Enchantment.registerEnchantment(ench);
      }
      catch (IllegalArgumentException e)
      {
        e.printStackTrace();
      }
      return;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
