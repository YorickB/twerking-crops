package Spigot.TwerkingCrops.Enchantments;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class CustomEnchantment
  extends Enchantment
  implements Listener
{
  public CustomEnchantment(int id)
  {
    super(id);
  }
  
  public int getId()
  {
    return 101;
  }
  
  public boolean canEnchantItem(ItemStack arg0)
  {
    return true;
  }
  
  public boolean conflictsWith(Enchantment arg0)
  {
    return false;
  }
  
  public EnchantmentTarget getItemTarget()
  {
    return null;
  }
  
  public int getMaxLevel()
  {
    return 3;
  }
  
  public String getName()
  {
    return "Crop Grower";
  }
  
  public int getStartLevel()
  {
    return 1;
  }
}
