package Spigot.TwerkingCrops.ReflectionUtil;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

public interface ReflectionUtil {
	
	public void applyBoneMeal(Location loc);
	public Class<?> getNmsClass(String name) throws ClassNotFoundException;

	public Class<?> getCraftBukkitClass(String name) throws ClassNotFoundException;
	public Object itemStackAsNmsCopy(ItemStack itemStack);
	public Object craftWorldGetHandle(World world);
	public Object constructBlockPosition(int x, int y, int z);
}
