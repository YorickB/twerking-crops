package Spigot.TwerkingCrops.ReflectionUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import Spigot.TwerkingCrops.Core;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;

public class ReflectionUtil_1_13 implements ReflectionUtil {

	private Class<?> nmsItemBoneMeal;
	private Class<?> nmsItemStack;
	private Class<?> nmsWorld;
	private Class<?> nmsBlockPosition;
	private Class<?> nmsEnumDirection;

	private Class<?> craftItemStack;
	private Class<?> craftWorld;

	private Constructor<?> nmsBlockPositionConstructor;

	private Method nmsItemBoneMealApply;
	private Method nmsItemBoneMealUnderwaterApply;
	private Method craftItemStackAsNmsCopy;
	private Method craftWorldGetHandle;

	private final String NMS_NAMESPACE = "net.minecraft.server";
	private final String CRAFTBUKKIT_NAMESPACE = "org.bukkit.craftbukkit";

	private String VERSION;

	@SuppressWarnings("rawtypes")
	private final static Class[] NO_ARGUMENTS = new Class[0];

	public ReflectionUtil_1_13() {
		String path = Bukkit.getServer().getClass().getPackage().getName();
		VERSION = path.substring(path.lastIndexOf('.') + 1);

		try {
			nmsItemBoneMeal = getNmsClass("ItemBoneMeal");
			nmsItemStack = getNmsClass("ItemStack");
			nmsWorld = getNmsClass("World");
			nmsBlockPosition = getNmsClass("BlockPosition");
			nmsEnumDirection = getNmsClass("EnumDirection");

			craftItemStack = getCraftBukkitClass("inventory.CraftItemStack");
			craftWorld = getCraftBukkitClass("CraftWorld");

			nmsBlockPositionConstructor = nmsBlockPosition.getConstructor(int.class, int.class, int.class);

			nmsItemBoneMealApply = nmsItemBoneMeal.getMethod("a", nmsItemStack, nmsWorld, nmsBlockPosition);
			nmsItemBoneMealUnderwaterApply = nmsItemBoneMeal.getMethod("a", nmsItemStack, nmsWorld, nmsBlockPosition, nmsEnumDirection);
			craftItemStackAsNmsCopy = craftItemStack.getMethod("asNMSCopy", ItemStack.class);
			craftWorldGetHandle = craftWorld.getMethod("getHandle", NO_ARGUMENTS);
		} catch (Exception e) {
			Core.getInstance().getLogger().log(Level.SEVERE,
					"Error loading NMS Classes, are you using the right version?", e);
		}
	}

	@Override
	public Class<?> getNmsClass(String name) throws ClassNotFoundException {
		return Class.forName(NMS_NAMESPACE + "." + VERSION + "." + name);
	}

	@Override
	public Class<?> getCraftBukkitClass(String name) throws ClassNotFoundException {
		return Class.forName(CRAFTBUKKIT_NAMESPACE + "." + VERSION + "." + name);
	}

	@Override
	public Object itemStackAsNmsCopy(ItemStack itemStack) {
		try {
			return craftItemStackAsNmsCopy.invoke(null, itemStack);
		} catch (IllegalAccessException | InvocationTargetException e) {
			Core.getInstance().getLogger().log(Level.SEVERE, "Error creating NMS ItemStack", e);
		}
		return null;
	}

	@Override
	public Object craftWorldGetHandle(World world) {
		try {
			return craftWorldGetHandle.invoke(world);
		} catch (IllegalAccessException | InvocationTargetException e) {
			Core.getInstance().getLogger().log(Level.SEVERE, "Error getting NMS World", e);
		}
		return null;
	}

	@Override
	public Object constructBlockPosition(int x, int y, int z) {
		try {
			return nmsBlockPositionConstructor.newInstance(x, y, z);
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			Core.getInstance().getLogger().log(Level.SEVERE, "Error constructing Block Position", e);
		}
		return null;
	}

	@Override
	public void applyBoneMeal(Location Loc) {
		
		Object nmsWorld = craftWorldGetHandle(Loc.getWorld());
    	Object nmsItemStack = itemStackAsNmsCopy(new ItemStack(Loc.getBlock().getType()));
    	Object nmsBlockPosition = constructBlockPosition((int) Loc.getX(),(int) Loc.getY(),(int) Loc.getZ());
		
		try {
			nmsItemBoneMealApply.invoke(null, nmsItemStack, nmsWorld, nmsBlockPosition);
			for(Object constant : nmsEnumDirection.getEnumConstants()) {
				nmsItemBoneMealUnderwaterApply.invoke(null, nmsItemStack, nmsWorld, nmsBlockPosition, constant);
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			Core.getInstance().getLogger().log(Level.SEVERE, "Error applying bone meal!", e);
		}
	}
}

