package Spigot.TwerkingCrops.CustomTimer;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import Spigot.TwerkingCrops.Core;
import Spigot.TwerkingCrops.ToolBox;

/*
 * Created by Yorick, Last modified on: 14-1-2019
 */
public class CustomTimer {
	
	boolean reload = false;
	
	private void TwerkPerSecond() {
		new BukkitRunnable()
	      {
	        public void run()
	        {
	        	if(reload) this.cancel();
	        	if(ToolBox.checkFunctionState("TwerkPerSecond")) {
	        	 for (Player p : Bukkit.getOnlinePlayers()) {
	        		 if(Core.getInstance().TwerkData.get(p.getUniqueId()) != null) {
	        			 int twerkData = Core.getInstance().TwerkData.get(p.getUniqueId());
		        		 String TwerkAmount = Integer.toString(twerkData);
		        		 if(twerkData >= 2) {
		        			 String message = Core.getInstance().getConfig().getString("Messages.TwerkingPerSecond.Shifting").replace("%ShiftingRate%", TwerkAmount);	
		        			 Core.getInstance().actionBar.sendActionBar(p, ToolBox.cc(message));
		        		 }
		        		 Core.getInstance().TwerkData.remove(p.getUniqueId());
	        		 }
	        	 	}
	        	}
	        }
	      }.runTaskTimer(JavaPlugin.getPlugin(Core.class), 0L, 20);
	}
	@SuppressWarnings("deprecation")
	private void RunnableFunc(List<Location> LocList) {	  
		Random rand = new Random();
		for (int i = 0; i < LocList.size(); i++)
		{
			try {
				Location loc = LocList.get(i);
				byte Stage = loc.getBlock().getData();
				if(Stage < 7) {
					byte FinalStage = (byte) (Stage + (rand.nextInt(2) + 2));
					if(FinalStage > 7) FinalStage = 7;
					loc.getBlock().setData(FinalStage); 
				}
			} catch (IllegalArgumentException e) { 
			}
		}
	}
	
	public void startRunnables()
	  {
		//Twerking Per Second Function
		TwerkPerSecond();
		
		//Custom Timer Function
	    if (ToolBox.checkFunctionState("CustomTime")) {
		    if (Core.getInstance().getConfig().getString("CustomTime.CARROT") != null)
		    {
				int Time = Core.getInstance().getConfig().getInt("CustomTime.CARROT");
				new BukkitRunnable() {
					public void run() { RunnableFunc(Core.getInstance().CarrotLocation); }
				}.runTaskTimer(JavaPlugin.getPlugin(Core.class), 0L, Time * 20);
		    }
		    if (Core.getInstance().getConfig().getString("CustomTime.POTATO") != null)
		    {
		    	int Time = Core.getInstance().getConfig().getInt("CustomTime.POTATO");
		    	new BukkitRunnable() {
					public void run() { RunnableFunc(Core.getInstance().PotatoLocation); }
				}.runTaskTimer(JavaPlugin.getPlugin(Core.class), 0L, Time * 20);
		    }
		    if (Core.getInstance().getConfig().getString("CustomTime.SEEDS") != null)
		    {
		    	int Time = Core.getInstance().getConfig().getInt("CustomTime.SEEDS");
		    	new BukkitRunnable() {
					public void run() { RunnableFunc(Core.getInstance().WheatLocation); }
				}.runTaskTimer(JavaPlugin.getPlugin(Core.class), 0L, Time * 20);
		    }
		    if (Core.getInstance().getConfig().getString("CustomTime.BEETROOT") != null)
		    {
		    	if(!Core.getInstance().version.contains("_1_8")) {
		    		int Time = Core.getInstance().getConfig().getInt("CustomTime.BEETROOT");
		    		new BukkitRunnable() {
						public void run() { RunnableFunc(Core.getInstance().BeetrootLocation); }
					}.runTaskTimer(JavaPlugin.getPlugin(Core.class), 0L, Time * 20);
		    	}
		    }
	    }
	  }
	
	public boolean Restart() {
		try {
			ReloadRunnables();	
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}
	public void ReloadRunnables() {
		reload = true;
		Core.getInstance().getLogger().log(Level.WARNING, "Restarting ALL bukkit runnables from Twerking-Crops");
		reload = false;
		
		startRunnables();
	}
}
