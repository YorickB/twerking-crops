package Spigot.TwerkingCrops.CustomTimer;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import Spigot.TwerkingCrops.Core;

/*
 * Created by Yorick, Last modified on: 14-1-2019
 */
public class EventHandlerCustomTimer implements Listener {
	@EventHandler
	  public void Placeblock(BlockPlaceEvent e)
	  {
	    Material blk = e.getBlock().getType();
	    if (blk == Material.CARROT)
	    {
	      Core.getInstance().CarrotValue += 1;
	      Core.getInstance().CarrotLocation.add(new Location(e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ()));
	    }
	    if (blk == Material.POTATO)
	    {
	      Core.getInstance().PotatoValue += 1;
	      Core.getInstance().PotatoLocation.add(new Location(e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ()));
	    }
	    if (blk == Material.CROPS)
	    {
	      Core.getInstance().WheatValue += 1;
	      Core.getInstance().WheatLocation.add(new Location(e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ()));
	    }
	    if(!Core.getInstance().version.contains("v1_8")) {
		    if (blk == Material.BEETROOT_BLOCK)
		    {
		      Core.getInstance().BeetrootValue += 1;
		      Core.getInstance().BeetrootLocation.add(new Location(e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ()));
		    }
	    }
	  }
	  
	  @EventHandler
	  public void removeBlock(BlockBreakEvent e)
	  {
	    Material blk = e.getBlock().getType();
	    Location loc = e.getBlock().getLocation();
	    if (blk == null)
	    {
	      return;
	    }
	    if (blk == Material.CARROT)
	    {
	      for (int a = 0; a < Core.getInstance().CarrotLocation.size(); a++) {
	      	if(Core.getInstance().CarrotLocation.contains(loc)) {
	      		Core.getInstance().CarrotLocation.remove(loc); 
	    		Core.getInstance().CarrotValue = Core.getInstance().CarrotValue - 1;
	      	}
	      }
	    }
	    if (blk == Material.POTATO)
	    {
	      for (int a = 0; a < Core.getInstance().PotatoLocation.size(); a++) {
	          if(Core.getInstance().PotatoLocation.contains(loc)) {
	        	  Core.getInstance().PotatoLocation.remove(loc); 
	        	  Core.getInstance().PotatoValue = Core.getInstance().PotatoValue - 1;      
	        }
	      }
	    }
	    if (blk == Material.CROPS)
	    {
	      for (int a = 0; a < Core.getInstance().WheatLocation.size(); a++) {
	          if(Core.getInstance().WheatLocation.contains(loc)) {
	        	  Core.getInstance().WheatLocation.remove(loc); 
	        	  Core.getInstance().WheatValue = Core.getInstance().WheatValue - 1;
	        }
	      }
	    }
	    if(!Core.getInstance().version.contains("v1_8")) {
		    if (blk == Material.BEETROOT_BLOCK)
		    {
		      for (int a = 0; a < Core.getInstance().BeetrootLocation.size(); a++) {
		          if(Core.getInstance().BeetrootLocation.contains(loc)) {
		        	  Core.getInstance().BeetrootLocation.remove(loc); 
		        	  Core.getInstance().BeetrootValue = Core.getInstance().BeetrootValue - 1;
		          
		        }
		      }
		    }
	    }
	    Core.getInstance().cfgm.saveSeeds();
	    Core.getInstance().saveConfig();
	  }
}
