package Spigot.TwerkingCrops;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Spigot.TwerkingCrops.Commands.BookFunctie;
import Spigot.TwerkingCrops.Commands.SetFunctie;
import Spigot.TwerkingCrops.Enchantments.CustomEnchantment;
import Spigot.TwerkingCrops.Enchantments.MainEnchants;
import net.md_5.bungee.api.ChatColor;

/*
 * Created by Yorick, Created on: 14-1-2019
 */
public class ToolBox {

	 public static void createJSONFromMap(JSONObject JsonObj, String FileName) {
		  try {
			createFile(Core.getInstance().getDataFolder() + "/Data/" + FileName);
		} catch (IOException e1) {
			 Core.getInstance().getLogger().log(Level.SEVERE, "Failed to create Melon & Pumpkin .dat file...", e1);
			e1.printStackTrace();
		}
	  		
		    try (FileWriter file = new FileWriter(Core.getInstance().getDataFolder() + "/Data/" + FileName)) {
		    	file.write(JsonObj.toJSONString());
		    } catch (IOException e) {
		    	 Core.getInstance().getLogger().log(Level.SEVERE, "Failed to write Melon & Pumpkin data to .dat file...", e);
			}
		}
	 
	 public static void createFile(String fullPath) throws IOException {
		    File file = new File(fullPath);
		    if(file.exists()) {
		    	return;
		    }
		    file.getParentFile().mkdirs();
		    file.createNewFile();
		}

	 public static void SaveCropsToConfig() {
		  Core.getInstance().getConfig().set("Ints.CARROT", Core.getInstance().CarrotValue);
		  Core.getInstance().getConfig().set("Ints.POTATO", Core.getInstance().PotatoValue);
		  Core.getInstance().getConfig().set("Ints.SEEDS", Core.getInstance().WheatValue);
		  Core.getInstance().getConfig().set("Ints.BEETROOT", Core.getInstance().BeetrootValue);
		  
		  for(int x = 0; x < Core.getInstance().CarrotLocation.size(); x++) {
			  Core.getInstance().cfgm.getSeeds().set("CARROT." + x + ".x", (int) Core.getInstance().CarrotLocation.get(x).getX());
			  Core.getInstance().cfgm.getSeeds().set("CARROT." + x + ".y", (int) Core.getInstance().CarrotLocation.get(x).getY());
			  Core.getInstance().cfgm.getSeeds().set("CARROT." + x + ".z", (int) Core.getInstance().CarrotLocation.get(x).getZ());
		  }
		  for(int x = 0; x < Core.getInstance().PotatoLocation.size(); x++) {
			  Core.getInstance().cfgm.getSeeds().set("POTATO." + x + ".x", (int) Core.getInstance().PotatoLocation.get(x).getX());
			  Core.getInstance().cfgm.getSeeds().set("POTATO." + x + ".y", (int) Core.getInstance().PotatoLocation.get(x).getY());
			  Core.getInstance().cfgm.getSeeds().set("POTATO." + x + ".z", (int) Core.getInstance().PotatoLocation.get(x).getZ());
		  }
		  for(int x = 0; x < Core.getInstance().WheatLocation.size(); x++) {
			  Core.getInstance().cfgm.getSeeds().set("SEEDS." + x + ".x", (int) Core.getInstance().WheatLocation.get(x).getX());
			  Core.getInstance().cfgm.getSeeds().set("SEEDS." + x + ".y", (int) Core.getInstance().WheatLocation.get(x).getY());
			  Core.getInstance().cfgm.getSeeds().set("SEEDS." + x + ".z", (int) Core.getInstance().WheatLocation.get(x).getZ());
		  }
		  for(int x = 0; x < Core.getInstance().BeetrootLocation.size(); x++) {
			  Core.getInstance().cfgm.getSeeds().set("BEETROOT." + x + ".x", (int) Core.getInstance().BeetrootLocation.get(x).getX());
			  Core.getInstance().cfgm.getSeeds().set("BEETROOT." + x + ".y", (int) Core.getInstance().BeetrootLocation.get(x).getY());
			  Core.getInstance().cfgm.getSeeds().set("BEETROOT." + x + ".z", (int) Core.getInstance().BeetrootLocation.get(x).getZ());
		  }
		  Core.getInstance().cfgm.saveSeeds();
	  }
	 
	  public static void UpdateConfig(String Func, String... params)
	  {
	    if (Func.equalsIgnoreCase("Set"))
	    {
	      Core.getInstance().getConfig().set("Custom." + params[1], params[0].toUpperCase());
	      Core.getInstance().saveConfig();
	    }
	  }
	  public static String cc(String msg)
	  {
	    String Functions = "";
	    String Enchantments = "";
	    
	    List<String> functions2 = Core.getInstance().Functions;
	    Map<String, CustomEnchantment> Enchantments2 = Core.getInstance().Enchants;
	    String arr;
	    for (int i = 0; i < functions2.size(); i++)
	    {
	      arr = (String)functions2.get(i);
	      Functions = Functions + arr + "/";
	    }
	    for (String key : Enchantments2.keySet()) {
	      Enchantments = Enchantments + key + "/";
	    }
	    return ChatColor.translateAlternateColorCodes('&', msg
	      .replace("%Func%", SetFunctie.Func)
	      .replace("%Bool%", SetFunctie.Bool)
	      
	      .replace("%Enchant%", BookFunctie.Enchant)
	      .replace("%Level%", BookFunctie.Level)
	      
	      .replace("%Functions%", method(Functions))
	      .replace("%Enchantments%", method(Enchantments))
	      .replace("%Reason%", SetFunctie.Reason)
	      );
	  }
	  public static String method(String str)
	  {
	    if ((str != null) && (str.length() > 0) && (str.charAt(str.length() - 1) == '/')) {
	      str = str.substring(0, str.length() - 1);
	    }
	    return str;
	  }
	  public static void addFunction(String Name, String ConfigLocation, String State)
	  {
		Core.getInstance().Functions.add(Name);
		if(Core.getInstance().getConfig().getString(ConfigLocation) != null) {
			if (Core.getInstance().getConfig().getString(ConfigLocation).equals(State)) {
	    	Core.getInstance().getLogger().log(Level.INFO, Name + " succesfully disabled");
			} else {
				Core.getInstance().getLogger().log(Level.INFO, Name + " succesfully enabled");
			}
		} else {
			Core.getInstance().getLogger().log(Level.WARNING, Name + " failed to load!");
		}
	  }
	  
	  public static void SaveStemsToConfig() {
		  int i = 0;	  
		  Map<Object, Object> a = new HashMap<Object, Object>(); 
		  
		  if(Core.getInstance().StemToBlock.size() >= 1) {
			  for (Entry<Location, HashMap<Location, Material>> entry : Core.getInstance().StemToBlock.entrySet()) {			  
				  Location locStem = entry.getKey();
				  HashMap<Location, Material> BlockMap = entry.getValue();
				  
				  Location locBlock = null;
				  Material mat = null;
				  for (Entry<Location, Material> BlockEntry : BlockMap.entrySet()) {
					  locBlock = BlockEntry.getKey();
					  mat = BlockEntry.getValue();
				  }
				  try {
					  a.put(i + "-LocBlock-X", locBlock.getX());
					  a.put(i + "-LocBlock-Y", locBlock.getY());
					  a.put(i + "-LocBlock-Z", locBlock.getZ());
					  a.put(i + "-LocBlock-W", locBlock.getWorld().getName());
					  
					  a.put(i + "-LocStem-X", locStem.getX());
					  a.put(i + "-LocStem-Y", locStem.getY());
					  a.put(i + "-LocStem-Z", locStem.getZ());
					  a.put(i + "-LocStem-W", locStem.getWorld().getName());
					  
					  a.put(i + "-Material", mat.toString());
					  
					  a.put("Amount", i + 1);
					  i++;  
				  } catch (NullPointerException e) {
					  Core.getInstance().getLogger().log(Level.WARNING, "Did not succesfully safe all Melon & Pumpkin Data... (Report tis error if it missed data, other wise disregard)", e);
				  }
				}
		  }
		  
		  JSONObject JsonObj = new JSONObject(a);
		  createJSONFromMap(JsonObj, "Data.dat");
	  }
	  public static void LoadStemsFromConfig() {	
		  JSONParser parser = new JSONParser();
		  try {
			  Object obj = null;
			  try {
			 obj = parser.parse(new FileReader(Core.getInstance().getDataFolder() + "/Data/Data.dat"));
			  } catch  (FileNotFoundException e ) {}
			  JSONObject JsonObj = (JSONObject) obj;
			  long count;
			  
			  try {
				  count = (long) JsonObj.get("Amount");  
			  } catch (NullPointerException e) {
				  count = 0;
			  }
			 
			  
			  for(int i = 0; i < count; i++) {
				  String LocStemW = (String) JsonObj.get(i + "-LocStem-W");
				  double LocStemX = (double) JsonObj.get(i + "-LocStem-X");
				  double LocStemY = (double) JsonObj.get(i + "-LocStem-Y");
				  double LocStemZ = (double) JsonObj.get(i + "-LocStem-Z");
				  
				  String LocBlockW = (String) JsonObj.get(i + "-LocBlock-W");
				  double LocBlockX = (double) JsonObj.get(i + "-LocBlock-X");
				  double LocBlockY = (double) JsonObj.get(i + "-LocBlock-Y");
				  double LocBlockZ = (double) JsonObj.get(i + "-LocBlock-Z");

				  String BlockMat = (String) JsonObj.get(i + "-Material");
				  
				  Location LocBlock = new Location(Bukkit.getWorld(LocBlockW), LocBlockX, LocBlockY, LocBlockZ);
				  Location LocStem = new Location(Bukkit.getWorld(LocStemW), LocStemX, LocStemY, LocStemZ);
				  
				  HashMap<Location, Material> x = new HashMap<Location, Material>();
				  x.put(LocBlock, Material.getMaterial(BlockMat));
				  
				  Core.getInstance().BlockToStem.put(LocBlock, LocStem);
				  Core.getInstance().StemToBlock.put(LocStem, x);
			  }
		  } catch (IOException | ParseException e) {
			  Core.getInstance().getLogger().log(Level.SEVERE, "Failed to load Melon & Pumpkin data from .dat file...", e);
		  }
	  }
	  
	  public static void CheckFuncties()
	  {
	    addFunction("CustomTime", "Custom.CustomTime", "FALSE");
	    addFunction("Particles", "Custom.Particles", "FALSE");
	    addFunction("Randomizer", "Custom.Randomizer", "FALSE");
	    addFunction("CustomEnchants", "Custom.CustomEnchants", "FALSE");
	    addFunction("TwerkPerSecond", "Custom.TwerkPerSecond", "FALSE");
	    
	    Core.getInstance().Functions.add("Twerking");
	    if (!Bukkit.getServer().getVersion().contains("1.13")) {
	      Core.getInstance().Enchants.put("CropGrower", MainEnchants.ench);
	    }
	    if (Core.getInstance().getConfig().getString("Custom.CustomEnchants").equals("TRUE")) {
	      UpdateConfig("set", new String[] { "TRUE", "Randomizer" });
	    }
	  }	  
	  public static void InitLocations() {
		  Core.getInstance().CarrotLocation.clear();
		  Core.getInstance().PotatoLocation.clear();
		  Core.getInstance().WheatLocation.clear();
		  Core.getInstance().BeetrootLocation.clear();	  
		  
		  for (int i = 0; i < Core.getInstance().CarrotValue; i++) {
			  World locw = Bukkit.getWorld("world");
	          int locx = Core.getInstance().cfgm.getSeeds().getInt("CARROT." + i + ".x");
	          int locy = Core.getInstance().cfgm.getSeeds().getInt("CARROT." + i + ".y");
	          int locz = Core.getInstance().cfgm.getSeeds().getInt("CARROT." + i + ".z");
	          Location Loc = new Location(locw, locx, locy, locz);
	          Core.getInstance().CarrotLocation.add(Loc);
		  }
		  for (int i = 0; i < Core.getInstance().PotatoValue; i++) {
			  World locw = Bukkit.getWorld("world");
			  int locx = Core.getInstance().cfgm.getSeeds().getInt("POTATO." + i + ".x");
			  int locy = Core.getInstance().cfgm.getSeeds().getInt("POTATO." + i + ".y");
			  int locz = Core.getInstance().cfgm.getSeeds().getInt("POTATO." + i + ".z");
	          Location Loc = new Location(locw, locx, locy, locz);
	          Core.getInstance().PotatoLocation.add(Loc);
		  }
		  for (int i = 0; i < Core.getInstance().WheatValue; i++) {
			  World locw = Bukkit.getWorld("world");
			  int locx = Core.getInstance().cfgm.getSeeds().getInt("SEEDS." + i + ".x");
			  int locy = Core.getInstance().cfgm.getSeeds().getInt("SEEDS." + i + ".y");
			  int locz = Core.getInstance().cfgm.getSeeds().getInt("SEEDS." + i + ".z");
	          Location Loc = new Location(locw, locx, locy, locz);
	          Core.getInstance().WheatLocation.add(Loc);
		  }
		  for (int i = 0; i < Core.getInstance().BeetrootValue; i++) {
			  World locw = Bukkit.getWorld("world");
			  int locx = Core.getInstance().cfgm.getSeeds().getInt("BEETROOT." + i + ".x");
			  int locy = Core.getInstance().cfgm.getSeeds().getInt("BEETROOT." + i + ".y");
			  int locz = Core.getInstance().cfgm.getSeeds().getInt("BEETROOT." + i + ".z");
	          Location Loc = new Location(locw, locx, locy, locz);
	          Core.getInstance().BeetrootLocation.add(Loc);
		  }
	  }
	  
	  public static boolean checkFunctionState(String Function) {
		  if(Core.getInstance().getConfig().getString("Custom." + Function).equals("TRUE")) {
			  return true;
		  }
		return false;
	  }
}
