package Spigot.TwerkingCrops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import Spigot.TwerkingCrops.ActionBar.ActionBar;
import Spigot.TwerkingCrops.ActionBar.ActionBar_1_11_B;
import Spigot.TwerkingCrops.ActionBar.ActionBar_1_12_A;
import Spigot.TwerkingCrops.Commands.BookFunctie;
import Spigot.TwerkingCrops.Commands.SetFunctie;
import Spigot.TwerkingCrops.CustomTimer.CustomTimer;
import Spigot.TwerkingCrops.CustomTimer.EventHandlerCustomTimer;
import Spigot.TwerkingCrops.Enchantments.CustomEnchantment;
import Spigot.TwerkingCrops.Enchantments.MainEnchants;
import Spigot.TwerkingCrops.PlayerEvents.AnvilEvents;
import Spigot.TwerkingCrops.PlayerEvents.AnvilEvents_1_9_A;
import Spigot.TwerkingCrops.PlayerEvents.PlayerEvents;
import Spigot.TwerkingCrops.PlayerEvents.PlayerEvents_1_13;
import Spigot.TwerkingCrops.PlayerEvents.PlayerEvents_1_8;
import Spigot.TwerkingCrops.PlayerEvents.PlayerEvents_1_9_ABOVE;
import Spigot.TwerkingCrops.ReflectionUtil.ReflectionUtil;
import Spigot.TwerkingCrops.ReflectionUtil.ReflectionUtil_1_13;
import net.md_5.bungee.api.ChatColor;

/*
 * Created by Yorick, Last modified on: 14-1-2019
 */
public class Core extends JavaPlugin {
	private static Core instance = null;
	public ActionBar actionBar;
	public PlayerEvents playerEvents;
	public ReflectionUtil reflectionUtil;
	public AnvilEvents anvilEvents;
	
	public Permission playerPermission = new Permission("Twerk.use");
	public Permission staffPermission = new Permission("Twerk.staff");
	
	public String version;
	public ConfigManager cfgm;
	
	public List<String> Functions = new ArrayList<String>();
	public Map<String, CustomEnchantment> Enchants = new HashMap<String, CustomEnchantment>();
	  
	public int CarrotValue = 0;
	public int PotatoValue = 0;
	public int WheatValue = 0;
	public int BeetrootValue = 0;
	  
	public List<Location> CarrotLocation = new ArrayList<Location>();
	public List<Location> PotatoLocation = new ArrayList<Location>();
	public List<Location> WheatLocation = new ArrayList<Location>();
	public List<Location> BeetrootLocation = new ArrayList<Location>();
	  
	public HashMap<UUID, Integer> TwerkData = new HashMap<UUID, Integer>();
	public HashMap<Location, HashMap<Location, Material>> StemToBlock = new HashMap<Location, HashMap<Location, Material>>();
	public HashMap<Location, Location> BlockToStem = new HashMap<Location, Location>();
	public ArrayList<Material> Crops = new ArrayList<Material>();
	
	public static Core getInstance()
	{
		return instance;
  	}
	
	/*
	 * Will run when plugin gets initialized by Spigot/Bukkit
	 */
	public void onEnable() {
		setupNMS();
	    instance = this;
	    
	    getServer().getPluginManager().registerEvents(new EventHandlerCustomTimer(), this);
	    getServer().getPluginManager().registerEvents((Listener) playerEvents, this);
	    if(anvilEvents != null)
	    	getServer().getPluginManager().registerEvents((Listener) anvilEvents, this);
	    else
	    	getServer().getLogger().log(Level.WARNING, ChatColor.GOLD + "Custom Enchantments are not Supported on your current Server Version!");
	    
	    getConfig().options().copyDefaults(true);
	    saveDefaultConfig();
	    
	    cfgm = new ConfigManager();
	    cfgm.seeds();
	    cfgm.saveSeeds();
	    cfgm.reloadSeeds();
	    
	    CarrotValue = getInstance().getConfig().getInt("Ints.CARROT");
	    PotatoValue = getInstance().getConfig().getInt("Ints.POTATO");
	    WheatValue = getInstance().getConfig().getInt("Ints.SEEDS");
	    BeetrootValue = getInstance().getConfig().getInt("Ints.BEETROOT");
	    
	    ToolBox.InitLocations();
	    ToolBox.LoadStemsFromConfig();
	    ToolBox.CheckFuncties();
	    
	    CustomTimer timer = new CustomTimer();
	    timer.startRunnables();
	    Commands();
	    
	    PluginManager pmp = getServer().getPluginManager();
	    pmp.addPermission(this.playerPermission);
	    pmp.addPermission(this.staffPermission);
	    
	    if (!Bukkit.getServer().getVersion().contains("1.13")) {
	    	MainEnchants.Enable();
	    }
	}
	/*
	 * Will run when plugin gets disabled by Spigot/Bukkit
	 */
	public void onDisable()
	  {
		  
	   ToolBox.SaveCropsToConfig();
	   ToolBox.SaveStemsToConfig();
		  
	    saveConfig();
	    if (!Bukkit.getServer().getVersion().contains("1.13")) {
	      MainEnchants.Disable();
	    }
	  }
	
	/*
	 * Will Configure Commands
	 */
	  public void Commands()
	  {
	    getCommand("set").setExecutor(new SetFunctie());
	    getCommand("book").setExecutor(new BookFunctie());
	  }
	
	/*
	 * Will Implement Correct Classes and Imports to let 
	 * Twerking-Crops function properly on your server version!
	 * 
	 * Level.INFO > Every is Just Fine
	 * Level.SEVERE > HUGE ERROR
	 * Level.WARNING > Just a headsup if something aint working
	 */
	private void setupNMS() {
		if (CheckNMS()) {
		      getLogger().log(Level.INFO, "Succesfully enabled correct NMS Classes");
		  } else {
			  getLogger().log(Level.SEVERE, "Failed to find the NMS class corresponding to your version!");
			  getLogger().log(Level.SEVERE, "Please report this error to the Plugin Creator with Your server Version");
		      getServer().getPluginManager().disablePlugin(this);
		  }
	}
	/*
	 * This will return true or false depending if we found a NMS classes for the compatible
	 * server version, so every function is compatible!
	 */
	private boolean CheckNMS() {
		try {
	          version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
	      } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {
	          return false;
	      }

	      getLogger().info("Your server is running version " + version);
	      boolean NMS = true;
	      initLists(version);

	      if (version.equals("v1_8_R1")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_8();
	      } else if (version.equals("v1_8_R2")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_8();
	      } else if (version.equals("v1_8_R3")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_8();
	      } else if (version.equals("v1_9_R2")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_9_ABOVE();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	      } else if (version.equals("v1_10_R1")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_9_ABOVE();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	      } else if (version.equals("v1_11_R1")) {
	    	  actionBar = new ActionBar_1_11_B();
	    	  playerEvents = new PlayerEvents_1_9_ABOVE();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	      } else if (version.equals("v1_12_R1")) {
	    	  playerEvents = new PlayerEvents_1_9_ABOVE();
	    	  actionBar = new ActionBar_1_12_A();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	      } else if (version.equals("v1_13_R1")) {
	    	  playerEvents = new PlayerEvents_1_13();
	    	  actionBar = new ActionBar_1_12_A();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	    	  reflectionUtil = new ReflectionUtil_1_13();
	      } else if (version.equals("v1_13_R2")) {
	    	  playerEvents = new PlayerEvents_1_13();
	    	  actionBar = new ActionBar_1_12_A();
	    	  anvilEvents = new AnvilEvents_1_9_A();
	    	  reflectionUtil = new ReflectionUtil_1_13();
	      } else {
	    	  NMS = false;
	      }
	      return NMS;
	}
	
	/*
	 * Puts correct crops into Crops list corresponding to your server version
	 */
	  public void initLists(String version)
	  {
		  Crops.add(Material.SAPLING);
		  Crops.add(Material.SEEDS);
		  Crops.add(Material.POTATO);
		  Crops.add(Material.CARROT);
		  Crops.add(Material.MELON_STEM);
		  Crops.add(Material.PUMPKIN_STEM);
		  Crops.add(Material.CROPS);
		  Crops.add(Material.LONG_GRASS);
		  
		  if (version.equals("v1_9_R2")) {
			  Crops.add(Material.BEETROOT_BLOCK);
	      } else if (version.equals("v1_10_R1")) {
	    	 Crops.add(Material.BEETROOT_BLOCK);
	      } else if (version.equals("v1_11_R1")) {
	    	  Crops.add(Material.BEETROOT_BLOCK);
	      } else if (version.equals("v1_12_R1")) {
	    	  Crops.add(Material.BEETROOT_BLOCK);
	      } else if (version.equals("v1_13_R1")) {
	    	  Crops.add(Material.BEETROOT_BLOCK);
	      } else if (version.equals("v1_13_R2")) {
	    	  Crops.add(Material.BEETROOT_BLOCK);
	      } 
	  }
}
