package Spigot.TwerkingCrops.Reflection;

/*
 * Created by Yorick, Last modified on: 14-1-2019
 */
public interface IReflectionObject {
    <E extends IReflectionObject> E setAccessible(boolean value);

    ReflectionUtil newCall();
}