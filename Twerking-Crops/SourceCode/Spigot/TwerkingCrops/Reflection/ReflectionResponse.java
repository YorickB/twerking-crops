package Spigot.TwerkingCrops.Reflection;

/*
 * Created by Yorick, Last modified on: 14-1-2019
 */
public class ReflectionResponse<T> {

    private T object;

    public ReflectionResponse(T object) {
        this.object = object;
    }


    public boolean isValid() {
        return object != null;
    }


    public T get() {
        return object;
    }
}
