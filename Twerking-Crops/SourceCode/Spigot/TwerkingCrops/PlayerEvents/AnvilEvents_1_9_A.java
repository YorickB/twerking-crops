package Spigot.TwerkingCrops.PlayerEvents;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import Spigot.TwerkingCrops.Enchantments.MainEnchants;

public class AnvilEvents_1_9_A implements Listener, AnvilEvents {
	private ArrayList<String> loreEnch = new ArrayList<String>();
	
	  private boolean CheckItemsLeggings(ItemStack item) {
		  if(item.getType() == Material.LEATHER_LEGGINGS || item.getType() == Material.CHAINMAIL_LEGGINGS || item.getType() == Material.IRON_LEGGINGS || 
				  item.getType() == Material.GOLD_LEGGINGS || item.getType() == Material.DIAMOND_LEGGINGS) {
			  return true;
		  }
		  return false;
	  }
	  private boolean CheckItemsBooks(ItemStack item) {
		  if(item.getType() == Material.ENCHANTED_BOOK) {
			  return true;
		  }
		  return false;
	  }
	  private ItemStack NewItemStack(ItemStack ResultItem, int EnchantlevelItem1, int EnchantLevelItem2, int ResultItemEnchant) {
		  ItemStack item4 = new ItemStack(ResultItem.getType()); 
		  ItemMeta meta = item4.getItemMeta();
		  if(ResultItem.getEnchantmentLevel(MainEnchants.ench) < 3) {
			  meta.addEnchant(MainEnchants.ench, ResultItemEnchant, true);
			  this.loreEnch.clear();
			  
		  	  if (ResultItemEnchant == 1)
	            this.loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " I");
	          if (ResultItemEnchant == 2)
	            this.loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " II");
	          if (ResultItemEnchant == 3)
	            this.loreEnch.add(ChatColor.GRAY + MainEnchants.ench.getName() + " III");
	          
	          meta.setLore(this.loreEnch);
	          item4.setItemMeta(meta);
	          return item4;
		  }
		  return null;
	  }
	  
	  @SuppressWarnings("deprecation")
	  @EventHandler
	  public void onAnvilEventInv(InventoryClickEvent e)
	  {
	    Inventory inv = e.getInventory();
	    if ((inv instanceof AnvilInventory))
	    {
	      AnvilInventory anvil = (AnvilInventory)inv;
	      if (e.getSlot() == 2)
	      {
	        ItemStack stack = anvil.getItem(2);
	        if (stack != null)
	        {
	          e.setCursor(stack);
	          anvil.setItem(0, null);
	          anvil.setItem(1, null);
	          anvil.setItem(2, null);
	          return;
	        }
	      }
	    }
	  }
	  @EventHandler
	  public void onAnvilEvent(PrepareAnvilEvent e)
	  {
	    Inventory inv = e.getInventory();
	    if ((inv instanceof AnvilInventory))
	    {
	      AnvilInventory anvil = (AnvilInventory)inv;      
	      ItemStack[] items = anvil.getContents();
	      ItemStack item1 = items[0];
	      ItemStack item2 = items[1];
	      if ((item1 != null) && (item2 != null)) {    	  
	    	  if(CheckItemsBooks(item1) && CheckItemsBooks(item2)) {
	    		  if(item1.containsEnchantment(MainEnchants.ench) && item2.containsEnchantment(MainEnchants.ench)) {
		    		  int enchLevelItem1 = item1.getEnchantmentLevel(MainEnchants.ench);
					  int enchLevelItem2 = item2.getEnchantmentLevel(MainEnchants.ench);
					  int enchLevel = enchLevelItem1 + 1;
					  	
					  ItemStack ResultItem = NewItemStack(item1, enchLevelItem1, enchLevelItem2, enchLevel);
		              e.setResult(ResultItem);
		              return;
				  }
			  }
	    	  if(CheckItemsLeggings(item1) && CheckItemsBooks(item2)) {
	    		  if(item2.containsEnchantment(MainEnchants.ench)) {
		    		  int enchLevelItem1 = item1.getEnchantmentLevel(MainEnchants.ench);
					  int enchLevelItem2 = item2.getEnchantmentLevel(MainEnchants.ench);
					  int EnchLvl = enchLevelItem2 - enchLevelItem1;
					  if(EnchLvl != 0) {
						  int enchLevel = enchLevelItem2;
						  	
						  ItemStack ResultItem = NewItemStack(item1, enchLevelItem1, enchLevelItem2, enchLevel);
			              e.setResult(ResultItem);
			              return;
					  }
	    		  }
	    	  }
	    	  if(CheckItemsLeggings(item1) && CheckItemsLeggings(item2)) {
	    		  if(item1.containsEnchantment(MainEnchants.ench) && item2.containsEnchantment(MainEnchants.ench)) {
	    			  if(item1.getType() == item2.getType()) {
			    		  int enchLevelItem1 = item1.getEnchantmentLevel(MainEnchants.ench);
						  int enchLevelItem2 = item2.getEnchantmentLevel(MainEnchants.ench);
						  int enchLevel = enchLevelItem2 + 1;
						  	
						  ItemStack ResultItem = NewItemStack(item1, enchLevelItem1, enchLevelItem2, enchLevel);
			              e.setResult(ResultItem);
			              return;
	    			  }
	    		  }
	    	  }
		  }
	    }
	  }
	
}
