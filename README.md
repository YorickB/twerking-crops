## Installation Guide
Install the plugin and get it running in under 3 minutes

1. Download the source code, put the .jar file in your server plugins folder
2. Start up your server so the Plugin files get created. 
3. Shut the server down to configre some of the Functions you want to be enabled. 
4. Start your server back up and check the console if all the enabled functions have loaded.
5. Give the correct groups/players the permissions they need and have fun using this plugin on your server!

---

## Features:
Currently the plugin provides a function that allows you to grow crops faster with Twerking, twerk in a **3 block radius** of your crops and a Area of **3x3** (Your the Middel Point) Will be Bonemealed, Particle effects on the crops will show you wich one have been effected!
There is also an Custom Growth Timer, with this function you can set a specefied amount of x seconds per Crops to let them grow, so every 60 seconds ALL the carrots in the game will be Grown +1 Grow Stage. 

The Randomizer will give an special functionality for better experience, instead of always having Succes (Crops getting BoneMealed) this function will give it an **20% chance**, to increase this chance of having succes you can use the CustomEnchant function. 
How higher your Enchantment Level the higher the chance of succes, here is the list of chances, 
**Lvl 0: 20%, Lvl 1: 25%, Lvl 2: 33%, Lvl 3: 50%**

1. Twerking for Crops/Trees
2. Set Command to set Functions True or False
3. Growth Timer for custom growing Times per Crop
4. BoneMeal Particles on the crops that are effected
5. Randomizer so not every twerk is succesfull (20% Chance)
6. CustomEnchantment increases the random chance for succes
7. Twerking Per Second To Display in ActionBar how many times you twerked per second

---

## Avaible Commands
All the avaible commands are listed down below with the needed permission to use this command, also a small description is provided for each command.

1. */set <GrowthTimer/Twerking/Particles/etc> <True/False> (This command provides an function to set specefied functions to true/false)*
2. */book <CropGrower> <1/2/3> (This command provides the function of giving yourself the custom Enchanted Book)*

---

## Permions
To use Several functions/commands you need some basic permissions all the permissions are listed down below! We have made the permissions really easy, so it does not take ages to give a player the permission it needs.

1. Twerk.Use (To use the Twerk Function)
2. Twerk.Staff (For The Commands)

---